#! /bin/bash

if [ $# -ne 1 ]; then 
    echo "Introduce argumentos"
    exit 1
fi 

dir=$1

if [ ! -d "$dir" ]; then
   echo "El directorio $dir no existe"
   exit 1
fi

while IFS= read -r nombre_dir
do
  dir_nuevo="$dir/$nombre_dir"
    mkdir -p "$dir_nuevo"
    echo "Directorio $dir_nuevo creado"
done < crear.txt
