#! /bin/bash

if [ $# -eq 0 ];then 
    echo "Error: añade al menos un argumento"
    exit 1 
fi


for fich in "$@" ; do
if [ -f $fich ]; then
    echo "FICHERO: $fich" 
    num_car=$(wc -c < $fich)
    echo "NÚMERO DE CARACTERES: $num_car"
    echo ············
else
    echo  "Error: el $fich no es válido"
fi
done

