#! /bin/bash

if [ $# -ne 2 ]; then
    echo "ERROR: proporciona solo 2 argumentos"
    exit 1
fi 

dir=$1
fich=$2

if [ -d $dir ]; then 
    echo "El directorio es: $dir"
    ls -l $dir
else
    echo  "$dir no es un directorio"
fi 


if [ -f $fich ]; then
    echo "El fichero es: $fich"
    cat "$fich"
else 
    echo "$fich no es un fichero"
fi

