#! /bin/bash

if [ $# -eq 0 ]; then
    echo "ERROR: añadir argumentos"
    exit 1
fi

dir=$1

if [ -d $dir ]; then 
    echo "El directorio $dir ya existe"
    exit 1
fi
  
    mkdir $dir
    echo "Directorio $dir se ha creado"

