#! /bin/bash

if [ $# -eq 2 ]; then
   echo "ERROR: AÑADE ARGUMENTOS"
   exit 1
fi

fich_dir=$(ls)

for item in $fich_dir; do
   if [ -f $item ]; then
       echo "Contenido: $item"
       cat $item
   elif [ -d $item ]; then
        echo "Contenido: $item"
        ls "$item"
   fi
   echo "·········"
done
