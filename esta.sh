#! /bin/bash

if [ $# -eq 0 ]; then 
   echo  "NÚMERO DE ARGUMENTOS INCORRECTO"
   echo "Sintáxis: esta.sh nombre-nombreusuario"
   exit 1
fi

usuario=$1

if who | grep -w "$usuario" >/dev/null; then 
    echo "$usuario está conectado"
else 
    echo "Perdón, $usuario no está conectado"
fi
