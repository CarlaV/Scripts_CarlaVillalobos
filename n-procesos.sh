#! /bin/bash 

if [ $# -eq 0 ]; then 
   echo "Introducir parámetros"
   exit 1
fi 

usuario=$1
procesos=$(ps -u "$usuario" | wc -l )

if [ "$procesos" -lt 10 ]; then
   echo "Hay menos de 10 procesos ejecutándose"
elif [ "$procesos" -gt 10 ]; then 
   echo "Hay más de 10 procesos ejecutándose"
fi
